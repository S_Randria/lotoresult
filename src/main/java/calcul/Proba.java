/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calcul;

import constante.Constante;

/**
 * la classe signifiant Probabilite 
 * Cette classe sert a calculer la probabilite d'une boule selon les parametres suivantes:
 * <i> Le nombre de fois que la boule a ete tire au sort auparavant </i>
 * <i> Le numero de sa sortie lors du tirage </i>
 * <i> Sortie de la premiere ou le second tirage </i>
 * @author sanda
 */
public class Proba {
    private String[] resultat;
    private Place place;
    
    public Proba(int numtirage,int nombreTirage,String resultat){
        this.initialize(numtirage, nombreTirage);
        this.setResultat(resultat);
    }
    
    private void initialize (int numtirage,int nombreTirage) {
        this.setPlace(new Place(numtirage,nombreTirage));
    }

    
    private int getPourcentage(int personnel,int univers){
        return personnel * Constante.cent / univers;
    }
    
    private Place getPlace() {
        return place;
    }

    private void setPlace(Place place) {
        this.place = place;
    }
    
    public String[] getResultat() {
        return resultat;
    }

    private void setResultat(String resultat) {
        this.resultat = resultat.split(Constante.separateur);
    }
    
    /**
     * @param indice le numero de sortie d'une boule lors de son tirage
     * @return la probabilite que la boule en question merite
     */
    public int calculer(int indice){
        int numerateur,
                denominateur,
                reponse;
        Place place = this.getPlace();
        place.setIndice(indice);
        numerateur = place.getProbabilite();
        denominateur = place.getUnivers();
        reponse = this.getPourcentage(numerateur, denominateur);
        return reponse;
    }
}
