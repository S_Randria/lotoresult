/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resultat;

import constante.FonctionDate;
import dao.GetConnection;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sanda
 */
public class ListeResultat {
    
    private String id, entreprise, nome, chiffre;
    private Date daty;
    private int numtirage;
    private int nombretirage;

    public ListeResultat() {
    }

    private ListeResultat(String entreprise, String daty) throws Exception {
        this.setEntreprise(entreprise);
        this.setDaty(daty);
    }

    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEntreprise() {
        return entreprise;
    }

    public void setEntreprise(String entreprise) throws Exception {
        if(entreprise == null || entreprise.equals(""))
            throw new Exception("Entreprise undefined");
        else this.entreprise = entreprise;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getChiffre() {
        return chiffre;
    }

    public int getNombretirage() {
        return nombretirage;
    }

    public void setNombretirage(int nombretirage) {
        this.nombretirage = nombretirage;
    }

    public void setChiffre(String chiffre) {
        this.chiffre = chiffre;
    }

    public Date getDaty() {
        return daty;
    }

    private void setDaty(String daty) {
        Date newDaty = new FonctionDate(daty).getDaty();
        this.setDaty(newDaty);
    }
    public void setDaty(Date daty) {
        this.daty = daty;
    }

    public int getNumtirage() {
        return numtirage;
    }

    public void setNumtirage(int numtirage) {
        this.numtirage = numtirage;
    }

    public static Pourcentage pourcentage(String entreprise,String daty) throws Exception{
        Pourcentage pourcentage = null;
        Connection connection = null;
        try{
            List<ListeResultat> listeBase = new ArrayList();
            ListeResultat listeR = new ListeResultat(entreprise,daty);
            String condition = "";
            condition += " and entreprise like '"+listeR.getEntreprise()+"'";
            condition += FonctionDate.getReqDateBefore(listeR.getDaty());
            
            connection = GetConnection.ouvrir();
            listeBase = listeR.find (condition,connection);
            ListeResultat temp;
            int tailleListe = listeBase.size();
            pourcentage = new Pourcentage();
            
            for(int i=0; i< tailleListe; i++){
                temp = listeBase.get(i);
                pourcentage.add(temp.getNumtirage(),temp.getNombretirage(), temp.getChiffre());
            }
            pourcentage.divise();
        }
        finally {
            if(connection!=null) connection.close();
        }
        return pourcentage;
    }

    private List<ListeResultat> find (String condition,Connection connection) throws Exception{
/* initialisation d'un objet vide */
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try{
/* Exception si connection vide*/
            if(connection == null){
                throw new Exception("Connection invalide");
            }
/* si condition vide , affecter condition par "" */
            if(condition == null){
                condition = "";
            }
/* Préparer la requette */
            String requette = "select * from listeresultat where 1=1 "+condition;
/* ouvrir un statement , et un resultset correspondant */
            statement = connection.prepareStatement(requette);
/* exécuter la requette */
            resultSet = statement.executeQuery();
/* transformer les résultats venant de la base en List de l'objet ListeResultat */
            List<ListeResultat> listes = new ArrayList();
            ListeResultat temporaire ;
            while (resultSet.next()){
                temporaire = new ListeResultat();
                temporaire.setId(resultSet.getString("id"));
                temporaire.setDaty(resultSet.getDate("daty"));
                temporaire.setEntreprise(resultSet.getString("entreprise"));
                temporaire.setNome(resultSet.getString("nome"));
                temporaire.setChiffre(resultSet.getString("chiffre"));
                temporaire.setNumtirage(resultSet.getInt("numtirage"));
                temporaire.setNombretirage(resultSet.getInt("nombretirage"));
                listes.add(temporaire);
            }
/* retourner la nouvelle liste */
            return listes;
        }
        finally{
/* fermeture des ressources */
            if(resultSet != null){
                resultSet.close();
            }
            if(statement != null){
                statement.close();
            }
        }
    }
    
    public static void main(String[] args) throws SQLException {
        Connection connection = null;
        try{
            Pourcentage p = ListeResultat.pourcentage("ENT10", "");
            System.out.println(p);
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
        finally {
            if(connection!=null) connection.close();
        }
    }

    @Override
    public String toString() {
        return "id=" + id + ", entreprise=" + entreprise + ", nome=" + nome + ", chiffre=" + chiffre + ", daty=" + daty + ", numtirage=" + numtirage + ", nombretirage=" + nombretirage + '}';
    }


}
