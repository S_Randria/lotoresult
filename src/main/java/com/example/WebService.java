/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import resultat.ListeResultat;
import resultat.Pourcentage;

/**
 *
 * @author sanda
 */
@Controller
public class WebService {
    
    @GetMapping(value="/")
    public ResponseEntity <String> hello(){
        return new ResponseEntity<>("hello world sanda ws",HttpStatus.OK);
    }
    @GetMapping(value="/pronostique")
    public ResponseEntity <Pourcentage> pronostique(
        @RequestParam("entreprise") String entreprise,
        @RequestParam("daty") String daty) throws Exception{
        try {
            System.out.println("entreprise "+entreprise+",daty "+daty);
            Pourcentage pourcentage = ListeResultat.pourcentage(entreprise,daty);
            return new ResponseEntity<>(pourcentage,HttpStatus.OK);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        }
    }
}
