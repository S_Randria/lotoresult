/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  sanda
 * Created: 5 août 2021
 */

CREATE DATABASE lotov1;

CREATE TABLE entreprise (
	id text NOT NULL,
	nom text NOT NULL,
	site text NOT NULL,
	popularite int NULL,
	etat int NULL,
	CONSTRAINT entreprise_pk PRIMARY KEY (id)
);
CREATE TABLE resultat (
	id text NOT NULL,
	daty date NOT NULL,
	entreprise text NOT NULL,
	CONSTRAINT resultat_pk PRIMARY KEY (id),
	CONSTRAINT resultat_fk FOREIGN KEY (entreprise) REFERENCES entreprise(id)
);
CREATE TABLE resultatdetails (
	chiffre text NOT NULL,
	mere text NOT NULL,
	nbtirage int4 NULL DEFAULT 1,
	CONSTRAINT resultatdetails_fk FOREIGN KEY (mere) REFERENCES resultat(id)
);

CREATE TABLE annulation (
	id text NOT NULL,
	resultat text NOT NULL,
	daty date NOT NULL,
	motif text NULL,
	etat int NULL,
	CONSTRAINT annulation_pk PRIMARY KEY (id),
	CONSTRAINT annulation_fk FOREIGN KEY (resultat) REFERENCES resultat(id)
);

CREATE TABLE pronostique (
	id text NOT NULL,
	daty date NOT NULL,
	datydemande date NULL DEFAULT now(),
	entreprise text NOT NULL,
	CONSTRAINT pronostique_pk PRIMARY KEY (id),
	CONSTRAINT pronostique_fk FOREIGN KEY (entreprise) REFERENCES entreprise(id)
);
create table pronostiquedetails ( 
    chiffre text not null,
    mere text not null,
    nbtirage int4 null default 1,
    constraint resultatdetails_fk foreign key (mere) references pronostique(id) 
);
