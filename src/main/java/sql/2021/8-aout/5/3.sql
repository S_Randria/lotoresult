/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  sanda
 * Created: 5 août 2021
 */


create sequence seqentreprise start with 10;

INSERT INTO entreprise
(id, nom, popularite, etat, site)
VALUES('ENT'||nextval('seqentreprise'), 'LOTO', 0, 0, 'www.loto.com');

create sequence seqresultat start with 6;

INSERT INTO resultat (id,daty,entreprise) VALUES
	 ('RSLT1','2021-07-03','ENT10'),
	 ('RSLT2','2021-07-05','ENT10'),
	 ('RSLT3','2021-07-07','ENT10'),
	 ('RSLT4','2021-07-10','ENT10'),
	 ('RSLT5','2021-07-12','ENT10');

INSERT INTO resultatdetails (chiffre,mere,nbtirage) VALUES
	 ('2-13-19-33-48','RSLT1',1),
	 ('8-10-21-25-43','RSLT1',2),
	 ('3-32-36-43-45','RSLT2',2),
	 ('1-6-28-34-46','RSLT2',1),
	 ('8-18-37-44-46','RSLT3',1),
	 ('5-18-39-40-44','RSLT3',2),
	 ('3-38-42-43-46','RSLT4',1),
	 ('1-23-30-34','RSLT4',2),
	 ('8-30-31-37-38','RSLT5',1),
	 ('1-2-7-8-17','RSLT5',2);