/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  sanda
 * Created: 5 août 2021
 */
create view resultatvalide as
select
	id,
	daty,
	entreprise
from
	resultat
where
	id not in (
	select
		a.resultat
	from
		annulation a);

create view listeresultat as
select
	id,
	daty,
	entreprise,
	r.chiffre,
	r.nbtirage
from
	resultatdetails r
join resultatvalide r2 on
	r.mere = r2.id
order by
	daty desc;
