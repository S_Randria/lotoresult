/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  sanda
 * Created: 10 août 2021
 */
drop view listeresultat;

create or replace
view listeresultat as
select
	r2.id,
	r2.daty,
	r2.entreprise,
	e.nom as nomE,
	r.chiffre,
	r.numtirage,
	r2.nombretirage
from
	resultatdetails r
join resultatvalide r2 on
	r.mere = r2.id
left join entreprise e on
	r2.entreprise = e.id 
order by
	r2.daty desc;